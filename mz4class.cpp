#include "mz4class.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>

MZ4Class::MZ4Class()
{
    file = new QFile("C:\\QtProjects\\paintvariant.cpp");
    fileinstring = "";
    s1 = 0; s2 = 0; s3 = 0; s4 = 0; s5 = 0; s6 = 0; s7 = 0; s9 = 0;
    UniqueOperatorsNumber = 0;
    UniqueOperandsNumber = 0;
    OperatorsNumber = 0;
    OperandsNumber = 0;
    SeparatorsNumber = 0;

}

void  MZ4Class::initOperators()
{
    int i = 0;
    QFile op("C:\\QtProjects\\operators.txt");

    if (op.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&op);
        while (!in.atEnd())
        {
            Operators.push_back(in.readLine());
            i++;
        }
        op.close();
    }
}

void  MZ4Class::calcOperatorsNumber()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            for (int i = 0; i < Operators.toSet().count(); i++)
            {
                if (line.contains(Operators[i]))
                {
                    OperatorsNumber++;
                    if(!UniqueOperators.contains(Operators[i]))
                        UniqueOperators.append(Operators[i]);
                }
            }
            UniqueOperatorsNumber = UniqueOperators.toSet().count();
        }
        file->close();
    }
}

void  MZ4Class::calcSeparatorsNumber()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains(";"))
               SeparatorsNumber++;
        }
        file->close();
    }
}

void  MZ4Class::calcOperandsNumber()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList s;
            if(line.contains("+"))
            {
                s = line.split("+");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }
            if(line.contains("-"))
            {
                s = line.split("-");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }
            if(line.contains("*"))
            {
                s = line.split("*");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }
            if(line.contains("/"))
            {
                s = line.split("/");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }
            if(line.contains("="))
            {
                s = line.split("=");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }

             UniqueOperandsNumber = UniqueOperands.toSet().count();
        }
        file->close();
    }
}

void MZ4Class::MethodsNumber()
{
    QStringList lst = fileinstring.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
    s2 = lst.count()-1;

}

void MZ4Class::MeanMethodsStringsNumber()
{
    int i, s = 0;
    QStringList lst = fileinstring.split(QRegExp("(void|int|QString|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
    for(i = lst.count()-1; i >= 0; --i)
    {
        const QString& item = lst[i];
        s += item.count("\n");
    }
    s1=(float)s/(lst.count())-1;

}

void MZ4Class::ModulesNumber()
{
    QStringList lst = fileinstring.split(QRegExp("#include"),QString::SkipEmptyParts);
    s3 = lst.count()-1;
}

void MZ4Class::ClassesNumber()
{
    QStringList lst = fileinstring.split(QRegExp("class"),QString::SkipEmptyParts);
    int i, s = 0;
    for (i = lst.count()-1; i >= 0; --i)
    {
        const QString&item = lst[i];
        s += item.count("\n")-1;
    }
    s5 = lst.count();
}

void MZ4Class::MeanClassesStringsNumber()
{
    QStringList lst = fileinstring.split(QRegExp("class"),QString::SkipEmptyParts);
    int i, s = 0;
    for (i = lst.count()-1; i >= 0; --i)
    {
        const QString&item = lst[i];
        s += item.count("\n")-1;
    }
    s4 = (float)s/(lst.count());
}

//количество удаленных методов класса
void MZ4Class::NORM()
{
     QStringList lst = fileinstring.split(QRegExp("class"),QString::SkipEmptyParts);
     int i;
     for(i = lst.count()-1; i >= 0; --i)
     {
         const QString&item = lst[i];
         QStringList list=item.split("){",QString::SkipEmptyParts);
         s6 += item.count("this->");
     }
}

//количество методов, которые могут вызываться экземплярами класса
void MZ4Class::RFC()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains("::") && (line.contains("void") || (line.contains("int") || (line.contains("QString")))))
               s7++;
        }
        file->close();
    }
  s7 += s6;
}

//количество классов-наследников
void MZ4Class::NOC()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains("class") && line.contains(":") && (line.contains("public") || (line.contains("private") || (line.contains("protected")))))
               s9++;
        }
        file->close();
    }
}

QString  MZ4Class::TotalCalculation()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        fileinstring = QTextStream(file).readAll();
    }
    file->close();

    initOperators();
    calcOperatorsNumber();
    calcOperandsNumber();
    calcSeparatorsNumber();
    MethodsNumber();
    MeanMethodsStringsNumber();
    ModulesNumber();
    ClassesNumber();
    MeanClassesStringsNumber();
    NORM();
    RFC();
    NOC();

    QString total = "";
    total += QString::number(s1); total+= " "; //Среднее количество строк для функций (методов)
    total += QString::number(s2); total+= " "; //Среднее количество строк, содержащих исходный код для функций (методов)
    total += QString::number(s3); total+= " "; //Среднее количество строк для модулей
    total += QString::number(s4); total+= " "; //Среднее количество строк для классов
    total += QString::number(s5); total+= " "; //Количество классов
    total += QString::number(s6); total+= " "; //NORM
    total += QString::number(s7); total+= " "; //RFC
    total += QString::number(((UniqueOperatorsNumber+SeparatorsNumber)/2)*(OperandsNumber/UniqueOperandsNumber)); total+= " "; //Суммарная сложность всех методов класса
    total += QString::number(s9); total+= " "; //NOC

    return total;
}
