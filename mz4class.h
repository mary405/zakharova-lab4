#ifndef MZ4CLASS_H
#define MZ4CLASS_H

#include <QObject>
#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QTextcodec>
#include <QStringList>
#include <QDebug>
#include <math.h>

class MZ4Class
{
private:
    QFile *file;
    QString fileinstring;
    float s1;
    float s2;
    float s3;
    float s4;
    float s5;
    int s6;
    int s7;
    double s8;
    float s9;

    short OperatorsNumber;
    short OperandsNumber;

    short UniqueOperatorsNumber;
    short UniqueOperandsNumber;
    short SeparatorsNumber;
    QStringList Operators;
    QStringList UniqueOperators;
    QStringList UniqueOperands;
public:
    MZ4Class();
    void initOperators();
    void calcOperatorsNumber();
    void calcOperandsNumber();
    void calcSeparatorsNumber();
    QString TotalCalculation();
    void MeanMethodsStringsNumber();
    void MethodsNumber();
    void ModulesNumber();
    void MeanClassesStringsNumber();
    void ClassesNumber();
    void NORM();
    void RFC();
    void NOC();
    void WMC();
};

#endif // MZ4CLASS_H
