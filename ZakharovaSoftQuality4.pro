#-------------------------------------------------
#
# Project created by QtCreator 2015-11-28T21:46:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ZakharovaSoftQuality4
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mz4class.cpp

HEADERS  += mainwindow.h \
    mz4class.h

FORMS    += mainwindow.ui
