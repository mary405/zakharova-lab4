#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonExit_clicked()
{
    this->close();
}

void MainWindow::on_pushButtonMetrics_clicked()
{
    MZ4Class m;
    QString x, w;
    QString words[9];

    x = m.TotalCalculation();
    QStringList list1 = x.split(" ");
    for (int i = 0; i < 9; i++)
    {
        w = list1.at(i);
        words[i] = w;
    }

    ui->lineEditStringsNumber->setText(words[0]);
    ui->lineEditMethodsNumber->setText(words[1]);
    ui->lineEditModulesNumber->setText(words[2]);
    ui->lineEditClassesStringsNumber->setText(words[3]);
    ui->lineEditClassesNumber->setText(words[4]);
    ui->lineEditNORM->setText(words[5]);
    ui->lineEditRFC->setText(words[6]);
    ui->lineEditWMC->setText(words[7]);
    ui->lineEditNOC->setText(words[8]);
}
